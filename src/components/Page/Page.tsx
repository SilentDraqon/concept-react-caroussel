import React from "react";
import {PageProps} from "./Page.types";
import "./Page.scss";
import "../../fontfaces.scss";

const Page = ({children}: PageProps) => {

    return (
        <div className="page">
            <header className="header">
                <h1>Carousell Prototype</h1>
            </header>
            <main className="main">
                {children}
            </main>
            <footer className="footer">
                <h4 className="footer__headline">Thank you for using me ^^</h4>
            </footer>
        </div>
    )
}

export default Page
