import React from "react";
import CarousellArrow from './CarousellArrow';

export default {
  title: "CarousellArrow"
};

export const LeftArrow = () => <CarousellArrow 
  direction={"left"} 
  onclick={() => {}} 
/>

export const RightArrow = () => <CarousellArrow 
  direction={"right"} 
  onclick={() => {}} 
/>