import React from "react";
import {CarousellArrowProps} from "./CarousellArrow.types";
import "./CarousellArrow.scss";
import Arrow from "./assets/svg/longbracket.svg"

const CarousellArrow = ({direction, onclick}: CarousellArrowProps) => {

    const _onclick = () => {
        console.log("clicked on CarousellArrow")
        onclick(direction)
    }

    return (
        <div onClick={() => _onclick()} className={`carousellarrow__${direction}`}>
            <Arrow direction="left" className="longbracket_svg"/>
        </div>
    )
}

export default CarousellArrow
