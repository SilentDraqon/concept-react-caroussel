import React from "react";
import { CarousellItemProps } from "./CarousellItem.types";
import "./CarousellItem.scss";

const CarousellItem = ({ children, identifier, title }: CarousellItemProps) => {

    return (
        <div key={"item-" + identifier} className="carousell__item">
            <div className="carousell-item__header">
                <p className="carousell-item__header--title">{title}</p>
                <p className="carousell-item__header--slide_id">{identifier}</p>
            </div>
            {children}
        </div>
    )
}

export default CarousellItem
