# Carousell Item

Represents one Slide within the Carousell

___

## Import

You can import this component by using

    import {CarousellItem} from "react-carousell"