import React from "react";
import Page from "../Page/Page";
import Carousell from './Carousell';
import CarousellItem from "../CarousellItem/CarousellItem";

export default {
  title: "Carousell",
  parameters: {
    layout: "fullscreen"
  }
};

export const Primary = () => <Page
  children={<Carousell allCarousellItems={[
    <CarousellItem children={<img alt="dragon" src="/images/dragon1.png" />} identifier={0} />,
    <CarousellItem children={<img alt="dragon" src="/images/dragon2.png" />} identifier={1} />,
    <CarousellItem children={<img alt="dragon" src="/images/dragon3.png" />} identifier={2} />,
    <CarousellItem children={<img alt="dragon" src="/images/dragon4.png" />} identifier={3} />,
    <CarousellItem children={<img alt="dragon" src="/images/dragon5.png" />} identifier={4} />
]}/>}
/>