import React, { useEffect, useRef, useState, useCallback } from "react";
import { useInterval } from "../../hooks/useInterval";
import { CarousellProps } from "./Carousell.types";
import CarousellArrow from "../CarousellArrow/CarousellArrow";
import "./Carousell.scss";
import CarousellItemHeader from "../CarousellItemHeader/CarousellItemHeader";
import CarousellNavbar from "../CarousellNavbar/CarousellNavbar";

const Carousell = ({allCarousellItems}: CarousellProps) => {

    /* DOM Data */
    const carousselRoot = useRef(null)
    const [carousselItems, setCarousselItems] = useState(allCarousellItems)
    const [carousselUpdateData, updateCarousselData] = useState({
        scrollBehavior: "smooth",
        posLeft: 0,
        posTop: 0
    })

    /* Intervals */
    const [isIntervalRunning, setIsIntervalRunning] = useState(false);
    const [intervalCount, setIntervalCount] = useState(0)
    const [intervalDelay, setIntervalDelay] = useState(100);
    const [intervalDirection, setIntervalDirection] = useState("forwards")
    const intervalCallback = () => intervalMove()
    useInterval(intervalCallback, isIntervalRunning ? intervalDelay : null);

    const intervalMove = () => {
        console.log("Move Caroussel")
        if (intervalCount === 50) intervalStopMove()
        setIntervalCount(prevFrameCount => prevFrameCount + 1)
    }

    const intervalStartMove = () => {
        console.log("Start Caroussel")
        setIsIntervalRunning(true)
    }

    const intervalStopMove = () => {
        console.log("Stop Caroussel")
        setIsIntervalRunning(false)
        setIntervalCount(0)
    }

    /* Event Callback */
    const horizontalMovement = (direction, origin, speed) => {
        console.log("Carousell moves to the " + direction)
        updateCarousselData({
            posLeft: 
                  (direction === "left") ? carousselRoot.current.scrollLeft - speed
                :(direction === "right") ? carousselRoot.current.scrollLeft + speed : 0,
            posTop: 0,
            scrollBehavior: origin === "arrow" ? "smooth" : "instant"
        })
    }

    /* Handler */
    const KeyDownHandler = (event) => {
        console.log("Handle KeyDown")
        if (event.keyCode === 37 || event.keyCode === 66) horizontalMovement("left", "keyboard", 30);
        else if (event.keyCode === 39 || event.keyCode === 68) horizontalMovement("right", "keyboard", 30);
    };

    const ArrowClickHandler = (direction) => {
        horizontalMovement(direction, "arrow", 800)
    }

    /* Render Hooks */
    const mount = () => {
        console.log("Mount Carousell")
        carousselRoot.current.focus()
        window.addEventListener("keydown", (event) => KeyDownHandler(event))
        intervalStartMove()
    }

    const unmount = () => {
        window.removeEventListener("keydown", KeyDownHandler)
        intervalStopMove()
        console.log("Unmount Carousell")
    }
    
    useEffect(() => {
        mount()
        return () => unmount()
    }, [])

    useEffect(() => {
        updateCarousellScrollPos()
    }, [carousselUpdateData])

    /* Render Functions */
    const updateCarousellScrollPos = () => {
        carousselRoot.current.scrollTo({
            left: carousselUpdateData.posLeft,
            top: carousselUpdateData.posTop,
            behavior: carousselUpdateData.scrollBehavior
        })
    }

    /* Component DOM */
    return (
        <div className="carousell">
            <div className="carousell__fixed">
                <CarousellArrow onclick={ArrowClickHandler} direction="left" />
                <CarousellArrow onclick={ArrowClickHandler} direction="right" />
                <CarousellNavbar allCarousellItems={allCarousellItems} />
            </div>
            <div className="carousell__moving">
                <div ref={carousselRoot} className="carousell__items">
                    {carousselItems.map((item, id) => item)}
                </div>
            </div>
        </div>
    )
}

export default Carousell
