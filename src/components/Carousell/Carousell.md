# Carousell

The Container for the entire Carousell.

___

## Import

You can import this component by using

    import {Carousell} from "react-carousell"
___

## Design Link

<iframe title="Figma Design" src="https://www.figma.com/file/7J2am6RiqFREYO3lq8r6YG/React-Caroussel?node-id=156%3A463"></iframe>
