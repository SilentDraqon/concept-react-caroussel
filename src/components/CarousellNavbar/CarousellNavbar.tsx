import React from "react";
import {CarousellNavbarProps} from "./CarousellNavbar.types";
import "./CarousellNavbar.scss";

const CarousellNavbar = ({allCarousellItems}: CarousellNavbarProps) => {

    return (
        <div className="carousell__navbar">
            <div className="carousell__navbar-item">
                {allCarousellItems.length}
            </div>
        </div>
    )
}

export default CarousellNavbar
