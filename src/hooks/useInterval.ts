import React, { useState, useEffect, useRef } from "react"

export const useInterval = (callback, delay) => {
    const savedCallback: any = useRef();
    const isRunning = () => (delay !== null);

    useEffect(() => {
        savedCallback.current = callback;
    }, [callback]);

    useEffect(() => {
        const tick = () => savedCallback.current();
        if (isRunning()) {
            const id = setInterval(tick, delay)
            return () => clearInterval(id);
        }
    }, [delay]);
}