# React Caroussel
Interactive React Caroussel 

## Installation
- run the following command:
    
    ***npm install --save-dev draq-react-carousell***


## **Cycle Modes**
### *Ping-Pong / Bounded Cycle*
In this mode, the Carousell cycles back and forth the ***Carousell-Item-Set***
### *Endless Cycle*
In this mode, the Carousell can travel forth or back endlessly through the ***Carousell-Item-Set***:
After the last tile is reached, the Set repeats itself while scrolling in the same direction as before.

---
## **Interface Mode**
### *None*
Setting ***Interface Mode*** to ***none*** will hide ***all*** UI elements


---
## **Interface Options**

### *Show arrows*
Setting 
